import Vue from 'vue'
import VueRouter from 'vue-router'
import Menu from "../components/Menu";
import MyCommunity from "../components/Community/MyCommunity";
import EmpDetails from "../components/Emp/EmpDetails";
import EmpPower from "../components/Power/EmpPower";
import login from "../components/login";
import RoomCost from "../components/cost/RoomCost";
import CostDetails from "../components/cost/CostDetails";
import CarCost from "../components/cost/CarCost";
import CarDetails from "../components/cost/CarDetails";
import CostOrder from "../components/cost/CostOrder";
import Announcement from "../components/smartService/Announcement";
import Advertisement from "../components/smartService/Advertisement";
import MenuList from "../components/Menu/MenuList";
import welcome from "../components/welcome";
import OrganizeDetails from "../components/Emp/OrganizeDetails";
import EmpInsert from "../components/Emp/EmpInsert";
import CommunityDetails from "../components/Community/CommunityDetails";
import CheckCommunity from "../components/Community/CheckCommunity";
import MenuGroup from "../components/Menu/MenuGroup";




Vue.use(VueRouter)
const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      component: login
    },
    {
      path: '/Menu',
      component: Menu,
      redirect: '/welcome',
      children:[
        {
          path: '/welcome',
          component: welcome
        },
        {
          //我的小区路径
          path: '/Community/MyCommunity',
          component: MyCommunity
        },
        {
          //  员工信息
          path: '/Emp/EmpDetails',
          component: EmpDetails
        },
        {
          // 员工权限
          path: '/Power/EmpPower',
          component: EmpPower
        },
        {
          //  登录路径
          path: '/login',
          component: login
        },
        {
          //  费用管理
          //  房屋收费
          path: '/cost/RoomCost',
          component: RoomCost
        },
        {
          //  查看详情
          path: '/cost/CostDetails',
          component: CostDetails
        },
        {
          // 车位收费
          path: '/cost/CarCost',
          component: CarCost
        },
        {
          path: '/cost/CarDetails',
          component: CarDetails
        }
        ,
        {
          //费用账单
          path: '/cost/CostOrder',
          component: CostOrder

        },
        {
          //发布公告
          path: '/smartService/Advertisement',
          component: Advertisement
        },
        {
          //发布广告
          path: '/smartService/Announcement',
          component: Announcement
        },
        {
          //  菜单栏路径
          path:'/Menu/MenuList',
          component: MenuList
        },
        {
        //  组织信息
          path:'/Emp/OrganizeDetails',
          component: OrganizeDetails
        },
        {
        //  添加员工
          path:'/Emp/EmpInsert',
          component: EmpInsert
        },
        {
          //小区信息
          path: '/Community/CommunityDetails',
          component: CommunityDetails
        },
        {
        //  审核小区
          path:'/Community/CheckCommunity',
          component: CheckCommunity
        }
        ,
        {
          path: '/Menu/MenuGroup',
          component: MenuGroup
        },


      ]
    }
  ]
})

// 挂在路由导航守卫
router.beforeEach((to, from, next) => {
  // to将要访问的路径
  // from 从哪来
  // next 放行函数
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
