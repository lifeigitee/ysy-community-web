// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'


Vue.use(router);
Vue.use(ElementUi);
Vue.config.productionTip = false

import axios from 'axios'
// 配置请求根路径
axios.defaults.baseURL= 'http://java101.qicp.vip/'
Vue.prototype.$http= axios

Vue.config.productionTip = false

axios.interceptors.request.use(config => {
  // console.log(config)
  config.headers.Authorization = window.sessionStorage.getItem('token')
  config.headers.userId = window.sessionStorage.getItem('userId')

  return config;
})

/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App),
  components: { App },
  template: '<App/>'
})
  .$mount('#app')
